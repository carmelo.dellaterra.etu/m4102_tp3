package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

@Path("/pizze")
public class PizzaRessource {
	private static final Logger LOGGER = Logger.getLogger(PizzaRessource.class.getName());
	private PizzaDao pizze;

	@Context
	public UriInfo uriInfo;
	
	
	public PizzaRessource() {
		pizze = BDDFactory.buildDao(PizzaDao.class);
		pizze.createPizzaTable();
	}
	
	@GET
	public List<PizzaDto> getAll() {
	    LOGGER.info("IngredientResource:getAll");
	    List<PizzaDto> l = pizze.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
	    return l;
	  }
	
	@GET
	@Path("{id}")
	public PizzaDto getOnePizza(@PathParam("id") long id) {
		LOGGER.info("getOnePizza(" + id + ")");
		try {
			Pizza pizza = pizze.findById(id);
			return Pizza.toDto(pizza);
		}
		catch ( Exception e ) {
			// Cette exception générera une réponse avec une erreur 404
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@POST
	public Response createPizza(PizzaCreateDto pizzaCreateDto) {
		Pizza existing = pizze.findByName(pizzaCreateDto.getName());
		if ( existing != null ) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
		try {
			Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
			long id = pizze.insert(pizza.getName());
			pizza.setId(id);
			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

			return Response.created(uri).entity(pizzaDto).build();
		}
		catch ( Exception e ) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteIngredient(@PathParam("id") long id) {
		if ( pizze.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	    pizze.remove(id);
	    return Response.status(Response.Status.ACCEPTED).build();
	}
	
	@GET
	@Path("{id}/name")
	public String getIngredientName(@PathParam("id") long id) {
	    Pizza pizza = pizze.findById(id);
		if (pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}	        
		return pizza.getName();
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createIngredient(@FormParam("name") String name) {
	    Pizza existing = pizze.findByName(name);
	    if ( existing != null ) {
	        throw new WebApplicationException(Response.Status.CONFLICT);
	    }
	    
	    try {
	        Pizza pizza = new Pizza();
	        pizza.setName(name);
	        
	        long id = pizze.insert(pizza.getName());
	        pizza.setId(id);
	        PizzaDto pizzaDto = Pizza.toDto(pizza);

	        URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

	        return Response.created(uri).entity(pizzaDto).build();
	    }
	    catch ( Exception e ) {
	        e.printStackTrace();
	        throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
	    }
	}
	
}
