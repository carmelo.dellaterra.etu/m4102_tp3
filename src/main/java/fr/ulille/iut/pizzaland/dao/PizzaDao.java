package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

  @SqlUpdate("CREATE TABLE IF NOT EXISTS pizze (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
  void createPizzaTable();

  @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaIngredientsAssociation (pizza INTEGER,"
		  															+"ingredient INTEGER,"
		  															+"PRIMARY KEY(pizza, ingredient),"
		  															+"FOREIGN KEY(pizza) REFERENCES pizze(id),"
		  															+"FOREIGN KEY(ingredient) REFERENCES ingredient(id))")
  void createAssociationTable();

  @Transaction
  default void createTableAndIngredientAssociation() {
    createAssociationTable();
    createPizzaTable();
  }

  @SqlUpdate("DROP TABLE IF EXISTS pizze")
  void dropTablePizza();
  
  @SqlUpdate("DROP TABLE IF EXISTS pizzaIngredientsAssociation")
  void dropTableAssociations();
  
  
  @SqlUpdate("INSERT INTO pizze (name) VALUES (:name)")
  @GetGeneratedKeys
  long insert(String name);
  
  @SqlQuery("SELECT * FROM pizze")
  @RegisterBeanMapper(Pizza.class)
  List<Pizza> getAll();
  
  @SqlQuery("SELECT * FROM pizze WHERE id = :id")
  @RegisterBeanMapper(Pizza.class)
  Pizza findById(long id);

  @SqlQuery("SELECT * FROM pizze WHERE name = :name")
  @RegisterBeanMapper(Pizza.class)
  Pizza findByName(String name);

  @SqlUpdate("DELETE FROM pizze WHERE id = :id")
  void remove(long id);

}
