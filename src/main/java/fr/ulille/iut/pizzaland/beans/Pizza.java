package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.ArrayList;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private long id;
	private String name;
	private List<Ingredient> ingredients;
	
	public Pizza() {}
	
	public Pizza(long id, String name) {
		this.id = id;
		this.name = name;
		this.ingredients = new ArrayList<>();
	}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setIngredients(List<Ingredient> list) {
		this.ingredients = list;
	}
	
	public List<Ingredient> getIngredients() {
		return this.ingredients;
	}
	
	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setId(p.getId());
		dto.setName(p.getName());
		dto.setIngredients(p.getIngredients());
		
		return dto;
	}
	
	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());
		pizza.setIngredients(dto.getIngredients());
		return pizza;
	}
	
	  @Override
	  public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    Pizza other = (Pizza) obj;
	    if (id != other.id)
	        return false;
	    if (ingredients == null) {
	        if (other.ingredients != null)
	            return false;
	    } else if (!ingredients.equals(other.ingredients))
	        return false;
	    return true;
	  }
	  
	  @Override
	  public String toString() {
	    return "Ingredient [id=" + id + ", ingredients=" + ingredients.toString() + "]";
	  }
	  
	  public static PizzaCreateDto toCreateDto(Pizza pizza) {
		    PizzaCreateDto dto = new PizzaCreateDto();
		    dto.setName(pizza.getName());
		    dto.setIngredients(pizza.getIngredients());
		    return dto;
		}

		public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		    Pizza pizza = new Pizza();
		    pizza.setName(dto.getName());
		    pizza.setIngredients(dto.getIngredients());
		    return pizza;
		}
}
	
