package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {
	private String name;
	private List<Ingredient> ingredients;
	
	public PizzaCreateDto() {}
	
	public List<Ingredient> getIngredients(){
		return this.ingredients;
	}
	
	public void setIngredients(List<Ingredient> list) {
		this.ingredients = list;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
