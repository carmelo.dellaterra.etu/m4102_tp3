package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
	private long id;
	private String name;
	private List<Ingredient> ingredients;
	
	public PizzaDto() {}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setIngredients(List<Ingredient> list) {
		this.ingredients = list;
	}
	
	public List<Ingredient> getIngredients() {
		return this.ingredients;
	}
	
}
	